FROM golang:1.6

RUN apt-get update  && \
    apt-get install -y librbd-dev librbd1 librados-dev librados2

RUN go get github.com/ceph/go-ceph

CMD ["/bin/bash"]