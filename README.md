## make docker image for build
```
docker build -t ceph-build .
```

## enter develop environment
```
docker run -ti --rm \
    -v "$PWD"/ceph:/etc/ceph \
    -v "$PWD":/go/src/gitlab.com/code0x9/ceph-test \
    -w /go/src/gitlab.com/code0x9/ceph-test \
    ceph-build
```

## build
```
go build
```

## test upload
```
./ceph-test up SampleVideo_1280x720_10mb.mp4
```

## test download
```
./ceph-test down SampleVideo_1280x720_10mb.mp4
```
