package main

import (
	"bufio"
	"fmt"
	"github.com/ceph/go-ceph/rados"
	"io/ioutil"
	"log"
	"os"
	"path"
)

var (
	err error
)

const (
	CEPH_POOLNAME    = "testpool"
	CEPH_BUFFER_SIZE = 4096 * 4096
)

func connect() *rados.IOContext {
	var (
		conn  *rados.Conn
		ioctx *rados.IOContext
	)
	if conn, err = rados.NewConn(); err != nil {
		log.Panicf("failed to create ceph instance: %v", err)
	}
	if err = conn.ReadDefaultConfigFile(); err != nil {
		log.Panicf("failed to read ceph config: %v", err)
	}
	if err = conn.Connect(); err != nil {
		log.Panicf("failed to connect to ceph: %v", err)
	}
	log.Println("ceph connected")

	// open a pool handle
	if ioctx, err = conn.OpenIOContext(CEPH_POOLNAME); err != nil {
		log.Panicf("failed to open ceph IOContext: %v", err)
	}
	return ioctx
}

func main() {
	ioctx := connect()

	if len(os.Args) != 3 {
		fmt.Println(`usage: ./ceph-test <command> [filename]
  up: upload [filename]
  down: download [filename]`)
		os.Exit(2)
	}
	cmd := os.Args[1]
	filepath := os.Args[2]

	switch cmd {
	case "up":
		log.Printf("upload %v", filepath)
		upload(ioctx, filepath)
	case "down":
		log.Printf("download %v", filepath)
		download(ioctx, filepath)
	default:
		log.Panicf("unknown command %v", cmd)
	}
}

func upload(ioctx *rados.IOContext, filepath string) {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		log.Panicf("failed to read file %s: %v", filepath, err)
	}
	err = ioctx.Write(path.Base(filepath), data, 0)
	if err != nil {
		log.Panicf("failed to write file: %v", err)
	}
}

func download(ioctx *rados.IOContext, filename string) {
	os.Mkdir("out", os.ModeDir)
	f, err := os.Create("out/" + filename)
	if err != nil {
		log.Panicf("failed to create file: %v", err)
	}
	w := bufio.NewWriter(f)
	defer f.Close()
	buf := make([]byte, CEPH_BUFFER_SIZE)
	var offset uint64
	for {
		n_out, err := ioctx.Read(filename, buf, offset)
		if err != nil {
			log.Panicf("failed to read file: %v", err)
		}
		if n_out < CEPH_BUFFER_SIZE {
			buf = buf[0:n_out]
			w.Write(buf)
			break
		}
		w.Write(buf)
		offset += uint64(n_out)
	}
}
